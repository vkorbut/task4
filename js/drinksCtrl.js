Drink.controller('drinksCtrl',['$scope','$http','$rootScope','$interval', function($scope, $http, $rootScope, $interval){
	$http.post('view/getDrinks.php')
            .success(function (data) {
            	$scope.drinks = data;
            })
            .error(function () {
            	console.log("error");
            });
    $scope.time = $rootScope.rootSpirit/5*3600;
	$scope.addSpirit = function(spirit, volume){
        if($rootScope.currentSpirit< $rootScope.rootSpirit)
        	$rootScope.currentSpirit = $rootScope.currentSpirit+(spirit/10*volume*.79*0.9)/($rootScope.rootWeight*$rootScope.rootWater/100);
        else{
            alert("You are drunk!");
            $interval(function(){

                $scope.hours= ($scope.time/3600)-(($scope.time/3600)%1);
                $scope.minutes= (($scope.time-$scope.hours*3600)/60)-((($scope.time-$scope.hours*3600)/60)%1);
                $scope.seconds=$scope.time-$scope.hours*3600- $scope.minutes*60;
                $scope.passingParam="До отрезвления: "+$scope.hours+":"+$scope.minutes+":"+$scope.seconds;
                $scope.time=$scope.time-1;
                $rootScope.currentSpirit=$rootScope.currentSpirit-($rootScope.rootSpirit/5*3600/$scope.time);
              }, 1000);
        }
	}
    $scope.setFav = function(x){
            $http.post('view/setFav.php',x.name)
            .success(function (data) {
                $scope.drinks= data;
            })
            .error(function () {
                console.log("error");
            });
        }
    $scope.delDrink = function(x){
        $http.post('view/delDrink.php', x.name)
        .success(function(data){
            $scope.drinks= data;
        })
        .error(function(){

        });
    }
}])