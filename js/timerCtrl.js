Drink.controller('timerCtrl', ['$scope', '$timeout', function($scope, $timeout) {
	$scope.counter = 10700;
	var updateCounter = function() {
		$scope.counter--;
		$scope.hours = ($scope.counter / 3600) - (($scope.counter / 3600) % 1);
		$scope.minutes = (($scope.counter - $scope.hours * 3600) / 60) - ((($scope.counter - $scope.hours * 3600) / 60) % 1);
		$scope.seconds = $scope.counter - $scope.hours * 3600 - $scope.minutes * 60;
		$timeout(updateCounter(), 1000);
		
	};
	var decrement = function(){
        $rootScope.currentSpirit = $rootScope.currentSpirit - 1;
    }
    
}]);