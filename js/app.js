var Drink = angular.module('Drink', ["ngRoute"])
    .run(function($rootScope) {
    $rootScope.rootWeight=70;
    $rootScope.rootWater=70;
    $rootScope.rootSpirit=1;
    $rootScope.currentSpirit=0;
    var decrement = function(){
        $rootScope.currentSpirit = $rootScope.currentSpirit - 1;
    }
});
   Drink.config(function($routeProvider, $locationProvider) {
        $routeProvider
            .when('/',{
                templateUrl:'view/drink.html',
                controller: 'drinkCtrl'
            })
            .when('/rest',{
                templateUrl:'view/rest.html',
                controller: 'restCtrl'
            })
            .when('/setting',{
                templateUrl:'view/setting.html',
                controller: 'settingCtrl'
            })
            .when('/addDrinks',{
                templateUrl:'view/addDrinks.html',
                controller: 'addDrinksCtrl'
            })
            .when('/drinks',{
                templateUrl:'view/drinks.html',
                controller: 'drinksCtrl'
            })
            .when('/correctDrink',{
                templateUrl:'view/correctDrink.html',
                controller: 'correctDrinkCtrl'
            })
        $locationProvider.html5Mode(true);
    });
Drink.factory('Drink', function(){
    return { current: '' };
});